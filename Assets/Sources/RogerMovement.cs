﻿using UnityEngine;
using System.Collections;

public class RogerMovement : MonoBehaviour {

	public Animator animator;

	void Update() {
		if ( Input.GetKeyDown(KeyCode.LeftArrow) ) {
			animator.SetTrigger("Left");
		}
		if ( Input.GetKeyDown(KeyCode.RightArrow) ) {
			animator.SetTrigger("Right");
		}
		if ( Input.GetKeyDown(KeyCode.Q) ) {
			animator.SetTrigger("Action1");
		}
		if ( Input.GetKeyDown(KeyCode.W) ) {
			animator.SetTrigger("Action2");
		}
		if ( Input.GetKeyDown(KeyCode.E) ) {
			animator.SetTrigger("Action3");
		}
	}

}
